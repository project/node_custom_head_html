Node custom head html
------------------------
by Sergei Tarassov

Description
-----------
This is a small and efficient module that allows you to set custom html code
into head section (like meta, script, link tags or else). You need to set
proper permissions to admin module and edit form field "Custom html code for
head section". Enable any node type to allow custom html for head section on
settings page admin/settings/node-custom-head-html. Also setup permissions for 
user roles.

Installation 
------------
 * Copy the module's directory to your modules directory and activate the
 module.
